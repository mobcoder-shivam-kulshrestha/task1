import React from 'react'
import './Positive.css'
import icon1 from '../../Assests/Image/Icon.svg'
import icon2 from '../../Assests/Image/Layer_10.svg'
import icon3 from '../../Assests/Image/reading-book.svg'
import icon4 from '../../Assests/Image/presentation.svg'

export default function Positive() {
    return (
        <section className='positive'>
            <div className='container'>
                <div className='positive_main'>
                    <div className='positive_upper'>
                        <h2>Empowering Minds, Impacting Communities</h2>
                        <p>We’re becoming the premiere eLearning hub for remote workers and communities across the globe, offering localized content that reflects their various cultures, languages, and learning styles.</p>
                    </div>
                    <ul className='positive_lower'>
                        <li>
                            <img src={icon1} alt="no icon1" />
                            <h3>742+</h3>
                            <p>Active Learners</p>
                        </li>
                        <li className='active'>
                            <img src={icon2} alt="no icon2" />
                            <h3>65+</h3>
                            <p>Educators</p>
                        </li>
                        <li>
                            <img src={icon3} alt="no icon" />
                            <h3>X</h3>
                            <p>Courses Available</p>
                        </li>
                        <li>
                            <img src={icon4} alt="no icon" />
                            <h3>X</h3>
                            <p>Communities Reached</p>
                        </li>

                    </ul>
                </div>


            </div>
        </section>
    )
}
