import React from 'react'
import "./Hero.css"
import heroLogo from '../../Assests/Image/herologo.png'
import bannerimage from '../../Assests/Image/banner-img.png'
import hero1 from '../../Assests/Image/triangle-icon.png'
import hero2 from '../../Assests/Image/ractangle-box.png'

export default function Hero() {
    return (
        <section className='Hero'>
            <div className='container'>
                <div className='hero_main'>
                    <div className='hero_div'>
                        <img src={heroLogo} alt="no hero logo" />
                        <h1 className='hero_head'>Let's build  skills with <span>IFA</span> & learn without limits...</h1>
                        <p>Take your learning to the next level</p>
                    </div>
                    <div className='hero_div hero_bg'>
                        <img src={hero2} alt="no rectangle image"  className='rect1'/>
                        <img src={bannerimage} alt="no banner image" />
                        <img src={hero1} alt="no traingle image"  className='trang1'/>
                    </div>
                </div>

            </div>
        </section>
    )
}
