import React from 'react'
import Header from '../Header/Header'
import Hero from '../Hero_section/Hero'
import Empowering from '../Empowering/Empowering'
import Positive from '../Positive/Positive'

export default function Main() {
  return (
    <div>
        <Header/>
        <Hero/>
        <Empowering/>
        <Positive/>
    </div>
  )
}
