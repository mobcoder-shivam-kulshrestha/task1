import React from 'react'
import './Header.css'
import headerImg from '../../Assests/Image/1. Text_Logo.png'
export default function Header() {
  return (
    <div  className='Header_div'>
      <div className='container'>
        <nav class="nav">
          <input type="checkbox" id="nav-check" />
          <div class="nav-header">
            <div class="nav-title">
              <img src={headerImg} alt="" />

            </div>
          </div>
          <div class="nav-btn">
            <label for="nav-check">
              <span></span>
              <span></span>
              <span></span>
            </label>
          </div>

          <ul class="nav-list">
            <li><a href="#">Home</a></li>
            <li><a href="#">About us</a></li>
            <li><a href="#">Course</a></li>
            <li><a href="#">Contact</a></li>

          </ul>
        </nav>

      </div>
    </div>
  )
}
