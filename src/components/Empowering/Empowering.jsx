import React from 'react'
import './Empower.css'
import empowerimg from '../../Assests/Image/empower-img.png'
export default function Empowering() {
  return (
    <section className='empower'>
      <div className='container'>
        <div className='main_empower'>
          <div className='empower_div'>
            <img src={empowerimg} alt="no empower image" />
          </div>
          <div className='empower_div empower_div2'>
            <p className='empower_head'>Learn Anywhere, Anytime</p>
            <p className='empower_heading'>Positive Learning Experiences At Your Fingertips</p>
            <p className='empower_para'>Access digital educational content directly on your mobile device and interact with a learning bot through any one of your preferred social messaging platforms. Come collaborate with peers and educators from around the world, and exchange knowledge and ideas as life-long learners.</p>
            <button className='empower_btn'>REGISTER AS LEARNER</button>

          </div>
        </div>
      </div>
    </section>
  )
}
